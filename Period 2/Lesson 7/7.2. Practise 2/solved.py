somelist = ["al", "allow", "abba", "face", "wow", "46784", "public", 134]

# somelist siyahısında olan sətirlərin içərisindən uzunluğu 2 simvoldan çox olan, əvvəli və sonu eyni hərflə bitən sözləri print edin
# ================================================
for i in somelist:
    if type(i) is str and len(i) > 2  and i[0] == i[-1]:
        print(i)
# ================================================

# Verilmiş siyahıda olan təkrarlanmaları silin
# ================================================
somelist = [1,2,3,2,12,2,4,3,10,3,2,1]

print("before removing duplicates somelist was", somelist)
# müvəqqəti olaraq yeni və boş siyahı yaradırıq
newlist = []
# əsas siyahını sadalayırıq
for i in somelist:
    # əgər element yeni siyahıda yoxdursa onda onu yeni siyahıya əlavə edirik
    if i not in newlist:
        newlist.append(i)

# yeni siyahını əsas siyahıya mənimsədirik və yeni siyahının dəyişənini silirik
somelist = newlist
del newlist

print("after removing duplicates somelist is", somelist)
# ================================================

# Verilmiş siyahının elementlərindən bəziləri siyahıdır. Yoxlayın əgər o siyahılardan boş olanı varsa onları ana siyahıdan silin
# ================================================
somelist = [
    1,
    2,
    [],
    3, 
    [],
    [1,2,3],
    4,
    "TechAcademy",
    ["T", "A"],
    []
]

print("somelist before clearing was", somelist)
for i in somelist:
    if type(i) is list and not i:
        somelist.remove(i)
print("somelist after clearing is", somelist)       
# ================================================