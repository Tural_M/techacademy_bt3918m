# verilən siyahıların birində şəxs adları, digərində soyadları, sonuncuda isə doğulduğu illər qeyd edilmişdir. Bu ad, soyad və illəri yanaşı print edin
# =========================================================

# =========================================================

# input() vasitəsi ilə istifadəçidən növbə ilə müxtəlif şəxs adları və soyadları daxil etməsini tələb edin. Əgər istifadəçi quit sözü yazarsa onda daxiletməni dayandırmaq və bütün şəxslərin ad və soyadlarını print etmək lazımdır. Əgər ad və soyad təkrarlanarsa onda o şəxsi daxil etməmək və istifadəçiyə bu barədə xəbərdarlıq etmək lazımdır
# =========================================================

# =========================================================